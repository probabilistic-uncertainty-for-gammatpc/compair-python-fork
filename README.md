# ComPair Python Fork

Run parts of this code to regenerate some of the figures in [Low-Energy Electron-Track Imaging for a Liquid Argon Time-Projection-Chamber Telescope Concept using Probabilistic Deep Learning](https://arxiv.org/abs/2207.07805) by M. Buuck, A. Mishra, E. Charles, N. Di Lalla, O. Hitchcock, M.E. Monzani, N. Omodei, and T. Shutt.

This code is designed to be run on the SLAC computing systems, but with modification it can be run anywhere (just don't expect the `bsub` commands to work out-of-the-box :wink:).

The ComPair plots in the above paper were produced for just one choice of incident angle and energy: $\cos(\theta)=0.8,\ E=1.000\ \textrm{MeV}$. The flow is:

1. Use geometry file and `cosima` to produce a `.sim` file
2. Feed the `.sim` file into Tom Shutt's detector response code to produce an `.evta` file
    1. You can also have this code produce an `mctruth` file to enable additional plots with `EventAnalysis.py` later
3. Feed the `.evta` file into `revan` to produce a `.tra` file
4. Feed the `.tra` file into the `EventAnalysis.py` script to produce plots
    1. If using, also include the `mctruth` file produced by the detector response code