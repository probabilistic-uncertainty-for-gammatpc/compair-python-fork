#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test / example script. Read and process full sim file, create vectorized
representation, then add detector response while scanning over cases
of example study.

Created on Thu Dec  3 10:53:17 2020

@author: tshutt
"""
import os
import sys
import argparse

from detectormodel import sim_file_handling
from detectormodel import response_definition
from detectormodel import detector_response
from detectormodel import apra_2020_proposal_studies



#%%     What to do

#   Pixel readout trade study
study  =  apra_2020_proposal_studies.SpatialResolutionStudy()

#   Example files are on our shared google drive,
#   in the MEGALib data examples, under Custom Drift Chambers:
#https://drive.google.com/drive/u/0/folders/1lC7-E8e9jHuVqExRLuSypfs0tCtKO0hC   
# base_file_name = 'DriftChamberHexCellCrabOnly.inc1.id1'

usage = "usage: %(prog)s [options]"
description = "Convert sim to evta"

parser = argparse.ArgumentParser(usage, description=description)

parser.add_argument("-i", "--input", type=str, required=True,
                    help="Input file")

parser.add_argument("-o", "--output", type=str, required=True, 
                    help="Formatted output string")

args = parser.parse_args(sys.argv[1:])

input_file_name = args.input

#%%     Read, process and vectorize .sim file

events = sim_file_handling.parse_and_flatten_sim_file(input_file_name)

#%%     Loop over study cases and apply detector response for each

for nc in range(study.num_cases):
        
    #   Add study to default response
    response = response_definition.Response()
    response.add_study_case(study,nc)
    response.calculate()
    
    #   Add detector response to MC truth
    events = detector_response.AddDetectorResponse(events, response)
    output_file_name = args.output.format(tag=study.case_tag[nc])
    sim_file_handling.write_evta_file(events, output_file_name)




