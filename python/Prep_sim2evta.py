# Author: Niccolo Di Lalla (niccolo.dilalla@stanford.edu)
# Date: October 6th, 2021

# Before using this code, check geofile, Log_E, and angles parameters.
# This code will generate source files and runSim2evta.sh


import numpy as np
import math
import argparse
import os


#here put your source type
OneBeam= 'FarFieldPointSource'

#define your energies and angles 
Log_E=[2.2,2.5,2.7,3,3.2,3.5,3.7,4,4.2,4.5,4.7,5,5.2,5.5,5.7,6,6.2,6.5,6.7]
angles  =[0,25.8,36.9,45.6,53.1,60]


#gives the cosTheta array
def ang2cos(allAng):
    ang =[]
    for i in allAng:
        a= round(np.cos(math.radians(i)),1)
        ang.append(a) 
    return ang

#in keV [316,501,1000,1585, ... ]
def logE2ene(allEne):
    ene =[]
    for ee in allEne:
        a=int(10**ee)
        ene.append(a) 
    return ene

energies=logE2ene(Log_E)
cos_ang =ang2cos(angles)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", default='/gpfs/slac/kipac/fs1/g/MeV/users/ndilalla/compair_python/data', help="Source file dir")
    parser.add_argument("-b", default='/gpfs/slac/kipac/fs1/g/MeV/users/ndilalla/compair_python/python', help="evta_to_sim.py bin dir")
    args = parser.parse_args()

    data_dir = args.f
    bin_dir = args.b
    tag_key = '{tag}'

    args = parser.parse_args()
    with open("./runSim2evta.sh", mode='w') as f:
        for myene in energies:
            for cosTh,ang in zip(cos_ang,angles):

                source_file = '%s_%.3fMeV_Cos%.1f'%(OneBeam,myene/1000.,cosTh)
                
                f.write('bsub -R "centos7" -W 6000 -o {}.sim2evta.log  {}/sim_to_evta.py -i {}/{}.inc1.id1.sim -o {}/{}_{}.evta\n'.format(source_file, bin_dir, data_dir, source_file, data_dir, source_file, tag_key))
